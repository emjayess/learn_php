<?php
/**
 * code samples from the 'namespaces' docs at php.net...
 * -->http://us2.php.net/manual/en/language.namespaces.php
 *
 * see also: http://phpmaster.com/php-namespaces/
 */

namespace foo;
    class Cat 
    {
        static function says() {
            echo 'meoow';
        }
    }

namespace bar;
    class Dog
    {
        static function says() {
            echo 'ruff';
        }
    }

namespace animate 
    class Animal
    {
        static function breathes() {
            echo 'air';
        }
    }

namespace runtime;
    // include the above as separate code files
    use foo as feline,
        bar as canine,
        animate;
        
    echo \feline\Cat::says(), "<br />\n";
    echo \canine\Dog::says(), "<br />\n";
    echo \animate\Animal::breathes(), "<br />\n";
//