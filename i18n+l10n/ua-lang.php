<?php

  /*
    must have:
      determine presence (or absence) of Accept-Language HTTP request header
      de-reference the weighted values of the Accept-Language header (q values)
      find the best match of accepted languages supported languages :)

    nice to have:
      set a cookie (session?) reflecting the determined best language
      and use the cookie on return visits.

      alternatively use localstorage technique

      machine translate the greeting string (bing or google translate api?)

    don't care:
      provide a widget for the user to choose an override for this site
      capture in the cookie
  */

  $prefs = array();

  // note: this does NOT meet the above requirements...
  // example taken from http://stackoverflow.com/questions/3770513/detect-browser-language-in-php
  $ualanguages = isset($_SERVER['HTTP_ACCEPT_LANGUAGE')
               ? $_SERVER['HTTP_ACCEPT_LANGUAGE']
               : '';

  // distinguish locale from language
  preg_match_all(
    '/([a-z]{2})(?:-[a-zA-Z]{2}])?/'),
    $ualanguages,
    $_prefs
  );

  foreach ($_prefs[0] as $locale) {
    $pref['locales'][] = $locale;
  }

  foreach ($_prefs[1] as $language) {
    $prefs['languages'] = $language;
  }

  switch ( $prefs['languages'][0] /*substr($ualanguages, 0, 2)*/ ) {
    case 'fr':
      echo 'prefers french';
      break;
    case 'it':
      echo 'prefers italiano';
      break;
    case 'en':
      echo 'prefers english';
      break;
    default:
      echo 'defaulting to english';
      break;
  }