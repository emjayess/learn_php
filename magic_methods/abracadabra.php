<?php
// see also: http://thinkvitamin.com/code/9-magic-methods-for-php/
//
// overloading
class PropertyTest
{
    /* location for overloaded data **/
    private $data = array();

    /* overloading not used on declared properties */
    public $declared = 1;

    /* overloading only used on this when accessed outside the class */
    private $hidden = 2;

    public function __set($name, $value) {
        echo "setting '$name' to '$value'\n";
        $this->data[$name] = $value;
    }

    public function __get($name) {
        echo "getting '$name'\n";
        if (array_key_exists($name, $this->data)) {
            return $this->data[$name];
        }

        $trace = debug_backtrace();
        trigger_error(
            'undefined property via __get(): ' . $name .
            ' in ' . $trace[0]['line'],
            E_USER_NOTICE
        );
        return null;
    }

    /* as of PHP 5. 1. 0 */
    public function __isset($name) {
        echo "is '$name' set?\n";
         return isset($this->data[$name]);
    }

    /* as of PHP 5. 1. 0 */
    public function __unset($name) {
        echo "unsetting '$name'\n";
        unset($this->data[$name]);
    }

    /* not a magic method, just here for example */
    public function getHidden() {
        return $this->hidden;
    }
}

echo "<pre>\n";
$test = new PropertyTest;

$test->a = 1;
echo $test->a . "\n\n";

var_dump(isset($test->a));
unset($test->a);
var_dump(isset($test->a));
echo "\n";

echo $test->declared . "\n\n";

echo "Let's experiment with the private property named 'hidden':\n";

echo "Privates are visible inside the class, so __get() not used...\n";
echo $test->getHidden() . "\n";

echo "Privates not visible outside the class, so __get() is used...\n";
echo $test->hidden . "\n";

echo "</pre>\n";
?>